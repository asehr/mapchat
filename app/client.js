var DEV = true;

$( document ).ready(function() {
  if(DEV) {
    getLocation();
  }
});

var userIcon = new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
                                                    new google.maps.Size(22,22),
                                                    new google.maps.Point(0,18),
                                                    new google.maps.Point(11,11));

// Determine client location
function getLocation() {
    if (navigator.geolocation) {
        $('#terms').html("Trying to get location");
        navigator.geolocation.getCurrentPosition(showPosition,showError);
    } else {
      $('#join_error').html('Geolocation is not supported by this browser.');
    }
}

function showPosition(position) {
    log("Show position");
    /*var latlon=position.coords.latitude+","+position.coords.longitude;

    var img_url="http://maps.googleapis.com/maps/api/staticmap?center="
    +latlon+"&zoom=14&size=400x300&sensor=false";*/
    log('Accuracy: ' + position.coords.accuracy);
    log("Latitude: " + position.coords.latitude);
    log("Longitude: " + position.coords.longitude);
    
    $('#enterPage').hide();
    showUserPosition(position.coords, position.coords);
}

function showError(error) {
  var msg = "Unknown Error";
  switch(error.code) {
    case error.PERMISSION_DENIED:
      msg = "Application not allowed to access location";
      break;
    case error.POSITION_UNAVAILABLE:
      msg = "Location information is unavailable.";
      break;
    case error.TIMEOUT:
      msg = "The request to get your location timed out.";
      break;
    default:
    case error.UNKNOWN_ERROR:
      msg = "An unknown error occurred.";
      break;
    }
    $('#join_error').html(msg);
    log(msg);
}

// Goolge maps helper functions
var map;

function initialize() {
    $("#dbglog").click(function() {
      $("#dbglog").toggleClass('fullscreen');
    });
    log('Init started\n');
    
    var mapOptions = {
        zoom: 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('mapholder'), mapOptions);

    // Try HTML5 geolocation
    if(!navigator.geolocation) {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    } else {
        // Stockholm
        map.setCenter(new google.maps.LatLng(59, 18));
    }
    log('Init done');
}

function showUserPosition(coords) {
    log('Show user position');

    var pos = new google.maps.LatLng(coords.latitude, coords.longitude);
    var accuracy = coords.accuracy;

    //if(!userMarker) {
        var userMarker = new google.maps.Marker({
            map: map,
            position: pos,
            icon: userIcon
        });
        var marker_circle = new google.maps.Circle({
                strokeColor: '#4190da',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#4190da',
                fillOpacity: 0.25,
                map: map,
                center: pos,
                radius: accuracy / 2
            });
    //}
    
    //var distance = computeDistance(pos, pathPoints[currentPoint]);
    
    // Draw user in one move
    userMarker.setPosition(pos);
    marker_circle.setCenter(pos);
    marker_circle.setRadius(accuracy);
    //showDirection(pos, distance);

    // Shall we auto move map?
    //if(!paningActive) {
    //    map.panTo(pos);
    //    fitNodesToMap(pos, pathPoints[currentPoint], map, true);
    //}

    /*var infowindow = new google.maps.Marker({
        map: map,
        position: pos,
        animation: google.maps.Animation.DROP,
    });*/

    map.panTo(pos);
    map.setZoom(14);
    log('Map updated');
}

function handleNoGeolocation(errorFlag) {
    var content = 'Error: ';
    if (errorFlag) {
        content += 'The Geolocation service failed.';
    } else {
        content += 'Your browser doesn\'t support geolocation.';
    }
    log(content);

    var options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
    };

    var infowindow = new google.maps.InfoWindow(options);
    map.setCenter(options.position);
}

function log(txt) {
    $('#dbglog').html($('#dbglog').html() + txt + '\n');    
}

$(document).ready(initialize);

/*
var videos = [];
var PeerConnection = window.PeerConnection || window.webkitPeerConnection00 || window.webkitRTCPeerConnection;

function getNumPerRow() {
  var len = videos.length;
  var biggest;

  // Ensure length is even for better division.
  if (len % 2 === 1) {
    len++;
  }

  biggest = Math.ceil(Math.sqrt(len));
  while (len % biggest !== 0) {
    biggest++;
  }
  return biggest;
}

function subdivideVideos() {
  var perRow = getNumPerRow();
  var numInRow = 0;
  for (var i = 0, len = videos.length; i < len; i++) {
    var video = videos[i];
    setWH(video, i);
    numInRow = (numInRow + 1) % perRow;
  }
}

function setWH(video, i) {
  var perRow = getNumPerRow();
  var perColumn = Math.ceil(videos.length / perRow);
  var width = Math.floor((window.innerWidth) / perRow);
  var height = Math.floor((window.innerHeight - 190) / perColumn);
  video.width = width;
  video.height = height;
  video.style.position = "absolute";
  video.style.left = (i % perRow) * width + "px";
  video.style.top = Math.floor(i / perRow) * height + "px";
}

function cloneVideo(domId, socketId) {
  var video = document.getElementById(domId);
  var clone = video.cloneNode(false);
  clone.id = "remote" + socketId;
  document.getElementById('videos').appendChild(clone);
  videos.push(clone);
  return clone;
}

function removeVideo(socketId) {
  var video = document.getElementById('remote' + socketId);
  if (video) {
      videos.splice(videos.indexOf(video), 1);
      video.parentNode.removeChild(video);
  }
}

function addToChat(msg, color) {
  var messages = document.getElementById('messages');
  msg = sanitize(msg);
  if (color) {
    msg = '<span style="color: ' + color + '; padding-left: 15px">' + msg + '</span>';
  } else {
    msg = '<strong style="padding-left: 15px">' + msg + '</strong>';
  }
  messages.innerHTML = messages.innerHTML + msg + '<br>';
  messages.scrollTop = 10000;
}

function sanitize(msg) {
  return msg.replace(/</g, '&lt;');
}

function initFullScreen() {
  var button = document.getElementById("fullscreen");
  button.addEventListener('click', function(event) {
    var elem = document.getElementById("videos");
    //show full screen
    elem.webkitRequestFullScreen();
  });
}

function initNewRoom() {
  var button = document.getElementById("newRoom");

  button.addEventListener('click', function(event) {

      var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
      var string_length = 8;
      var randomstring = '';
      for (var i=0; i<string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum,rnum+1);
      }

      window.location.hash = randomstring;
      location.reload();
  });
}


var websocketChat = {
  send: function (message) {
    rtc._socket.send(message);
  },
  recv: function (message) {
    return message;
  },
  event: 'receive_chat_msg'
};

var dataChannelChat = {
  send: function (message) {
    for (var connection in rtc.dataChannels) {
      var channel = rtc.dataChannels[connection];
      channel.send(message);
    }
  },
  recv:  function (channel, message) {
    return JSON.parse(message).data;
  },
  event: 'data stream data'
};

function initChat() {
  var chat;

  if (rtc.dataChannelSupport) {
    console.log('initializing data channel chat');
    chat = dataChannelChat;
  } else {
    console.log('initializing websocket chat');
    chat = websocketChat;
  }

  var input = document.getElementById("chatinput");
  var room = window.location.hash.slice(1);
  var color = "#"+((1<<24)*Math.random()|0).toString(16);

  input.addEventListener('keydown', function(event) {
    var key = event.which || event.keyCode;
    if (key === 13) {
      chat.send(JSON.stringify({
        "eventName": "chat_msg",
        "data": {
        "messages": input.value,
        "room": room,
        "color": color
        }
      }));
      addToChat(input.value);
      input.value = "";
    }
  }, false);
  rtc.on(chat.event, function() {
    var data = chat.recv.apply(this, arguments);
    console.log(data.color);
    addToChat(data.messages, data.color.toString(16));
  });
}


function init() {
  if(PeerConnection){
    rtc.createStream({"video": true, "audio": true}, function(stream) {
      document.getElementById('you').src = URL.createObjectURL(stream);
      videos.push(document.getElementById('you'));
      //rtc.attachStream(stream, 'you');
      subdivideVideos();
    });
  }else {
    alert('Your browser is not supported or you have to turn on flags. In chrome you go to chrome://flags and turn on Enable PeerConnection remember to restart chrome');
  }


  var room = window.location.hash.slice(1);
  rtc.connect("ws:" + window.location.href.substring(window.location.protocol.length).split('#')[0], room);

  rtc.on('add remote stream', function(stream, socketId) {
    console.log("ADDING REMOTE STREAM...");
    var clone = cloneVideo('you', socketId);
    document.getElementById(clone.id).setAttribute("class", "");
    rtc.attachStream(stream, clone.id);
    subdivideVideos();
  });
  rtc.on('disconnect stream', function(data) {
      console.log('remove ' + data);
      removeVideo(data);
  });
  initFullScreen();
  initNewRoom();
  initChat();
}

window.onresize = function(event) {
  subdivideVideos();
};*/